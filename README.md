# Quarel VS Code Extension README

Use this extension to get syntax highlighting of QVR files (Quarrel language). Eventually will include language server functionality.

## Features

Currently just syntax highlighting.

## Requirements

You should have Quarrel installed on your system. This will be a bit hard until a working prototype is released.

## Known Issues

A Quarrel compiler is yet to be completed.

## Release Notes

### 0.0.2

Changed extension from `.ql` to `.qvr` due to Quarrel language convention change.

### 0.0.1

Initial batch of syntax highlighting rules.
